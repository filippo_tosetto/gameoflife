//
//  Array+Extensions.swift
//  GameOfLife
//
//  Created by Filippo Tosetto on 24/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import Foundation

extension Array {
  
  subscript (safe index: Int) -> Element? {
    return self.indices ~= index ? self[index] : nil
  }
}
