//
//  UITableView+Extensions.swift
//  GameOfLife
//
//  Created by Filippo Tosetto on 24/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit

extension UITableView {
  
  func registerClass<T: UITableViewCell>(_ cellClass: T.Type) {
    self.register(cellClass.self, forCellReuseIdentifier: String(describing: cellClass))
  }

  func registerNibClass<T: UITableViewCell>(_ nibClass: T.Type) {
    self.register(UINib(nibName: String(describing: nibClass), bundle: nil), forCellReuseIdentifier: String(describing: nibClass))
  }
  
  func dequeReusableCellWithClass<T: UITableViewCell>(_ cellClass: T.Type, forIndexPath: IndexPath) -> T? {
    return self.dequeueReusableCell(withIdentifier: String(describing: cellClass), for: forIndexPath) as? T
  }
  
}
