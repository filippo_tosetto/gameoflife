//
//  ApplicationCoordinator.swift
//  GameOfLife
//
//  Created by Filippo Tosetto on 24/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit

class ApplicationCoordinator: BaseCoordinator {
  
  private let window: UIWindow
  
  init(window: UIWindow) {
    self.window = window

    super.init()
  }
  
  override func start() {
    let navigationController = UINavigationController()
    let mainCoordinator = MainCoordinator(presenter: navigationController)

    self.store(coordinator: mainCoordinator)

    self.window.rootViewController = navigationController
    self.window.makeKeyAndVisible()

    mainCoordinator.isCompleted = { [weak self] in
      self?.free(coordinator: mainCoordinator)
    }
  }
}
