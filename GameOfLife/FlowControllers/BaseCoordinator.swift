//
//  BaseCoordinator.swift
//  GameOfLife
//
//  Created by Filippo Tosetto on 24/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit

class BaseCoordinator: Coordinator {
  
  var navigationController = UINavigationController()

  var childCoordinators: [Coordinator] = []
  var isCompleted: (() -> ())?

  func start() {
    fatalError("Children should implement `start`.")
  }
}
