//
//  Coordinator.swift
//  GameOfLife
//
//  Created by Filippo Tosetto on 24/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit

protocol Coordinator: class {
  var navigationController: UINavigationController { get set }
  
  var childCoordinators: [Coordinator] { get set }
  var isCompleted: (() -> ())? { get set }
  
  func start()
}

extension Coordinator {

  func store(coordinator: Coordinator) {
    self.childCoordinators.append(coordinator)
    coordinator.start()
  }

  func free(coordinator: Coordinator) {
    self.childCoordinators = self.childCoordinators.filter { $0 !== coordinator }
    coordinator.isCompleted = nil
  }
}
