//
//  DetailsCoordinator.swift
//  GameOfLife
//
//  Created by Filippo Tosetto on 24/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit

class DetailsCoordinator: BaseCoordinator {
  
  private var presenter: UINavigationController?
  public var viewModel: UniverseViewModel
  
  init(viewModel: UniverseViewModel, presenter: UINavigationController?) {
    self.presenter = presenter
    self.viewModel = viewModel

    super.init()

    self.viewModel.didTapBack = { [weak self] in
      self?.isCompleted?()
    }
  }

  override func start() {
    let detailsViewController = UniverseDetailsViewController(viewModel: self.viewModel)
    self.presenter?.pushViewController(detailsViewController, animated: true)
  }
}


