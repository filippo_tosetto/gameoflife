//
//  MainCoordinator.swift
//  GameOfLife
//
//  Created by Filippo Tosetto on 24/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit

class MainCoordinator: BaseCoordinator {
  
  private let presenter: UINavigationController?
  public var viewModel: UniversesListViewModel
  
  init(presenter: UINavigationController?) {
    self.presenter = presenter
    self.viewModel = UniversesListViewModel()

    super.init()

    self.viewModel.didSelect = { [weak self] universe in
      self?.showDetails(withUniverse: universe)
    }
  }

  override func start() {

    let listViewController = UniversesListViewController(viewModel: self.viewModel)
    self.presenter?.pushViewController(listViewController, animated: true)
  }
  
  private func showDetails(withUniverse universe: Universe)  {
    
    let detailsViewModel = UniverseViewModel(universe: universe)
    let detailsCoordinator = DetailsCoordinator(viewModel: detailsViewModel, presenter: self.presenter)

    self.store(coordinator: detailsCoordinator)

    detailsCoordinator.isCompleted = { [weak self] in
      self?.free(coordinator: detailsCoordinator)
    }
  }
}


