//
//  NameGenerator.swift
//  GameOfLife
//
//  Created by Filippo Tosetto on 27/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import Foundation

struct NameGenerator {
  
  private let names: [String]
  private let currentUsedNames: [String]
  
  init() {
    self.names = NAMES
    self.currentUsedNames = StorageManager().getAllUniverses().map { $0.name.replacingOccurrences(of: "_", with: " ") }
  }
  
  // Generate a random name from the list of mathematicians names
  func generateName() -> String {
    
    // Random element on the names array won't return nill as the collection is not empty
    // so I can safetely force unwrap it
    let newName = self.names.randomElement()!
    if self.currentUsedNames.contains(newName) {
      return self.generateName()
    }
    
    return newName
  }
}
