//
//  Storage.swift
//  GameOfLife
//
//  Created by Filippo Tosetto on 24/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import Foundation

class Storage {
  
  private let filemanager = FileManager.default
  
  // Store an encodable struct to the specified directory on disk
  func store<T: Encodable>(_ object: T, as fileName: String) {
    let url = self.getURL().appendingPathComponent(fileName, isDirectory: false)
    
    let encoder = JSONEncoder()
    do {
      let data = try encoder.encode(object)
      if self.fileExists(fileName) {
        try self.filemanager.removeItem(at: url)
      }
      self.filemanager.createFile(atPath: url.path, contents: data, attributes: nil)
    } catch {
      fatalError(error.localizedDescription)
    }
  }
    
  // Retrieve all encodable structs in the document directory
  func retrieveAll<T: Decodable>(as type: T.Type) -> [T] {
    let url = self.getURL()
    var models = [T]()
    
    do {
      
      let contents = try self.filemanager.contentsOfDirectory(at: url, includingPropertiesForKeys: nil, options: [])
      for fileUrl in contents {
        if let data = self.filemanager.contents(atPath: fileUrl.path) {
          let decoder = JSONDecoder()
          let model = try decoder.decode(type, from: data)
          models.append(model)
        }
      }
      
    } catch {
      fatalError(error.localizedDescription)
    }
    
    return models
  }
  
  // Retrieve an encodable structs in the document directory with specific filename
  func retrieve<T: Decodable>(_ fileName: String, as type: T.Type) -> T {
    let url = self.getURL().appendingPathComponent(fileName, isDirectory: false)
    
    if !self.fileExists(fileName) {
      fatalError("File at path \(url.path) does not exist!")
    }
    
    if let data = self.filemanager.contents(atPath: url.path) {
      let decoder = JSONDecoder()
      do {
        let model = try decoder.decode(type, from: data)
        return model
      } catch {
        fatalError(error.localizedDescription)
      }
    } else {
      fatalError("No data at \(url.path)!")
    }
  }
    
  // Remove all files in the document directory
  func clear() {
    let url = self.getURL()
    do {
      let contents = try self.filemanager.contentsOfDirectory(at: url, includingPropertiesForKeys: nil, options: [])
      for fileUrl in contents {
        try self.filemanager.removeItem(at: fileUrl)
      }
    } catch {
      fatalError(error.localizedDescription)
    }
  }
    
  // Remove file with name from documents directory
  func remove(_ fileName: String) {
    if self.fileExists(fileName) {
      do {
        let url = self.getURL().appendingPathComponent(fileName, isDirectory: false)
        try self.filemanager.removeItem(at: url)
      } catch {
        fatalError(error.localizedDescription)
      }
    }
  }
  
  // Check if file with name exists in the document directory
  func fileExists(_ fileName: String) -> Bool {
    let url = self.getURL().appendingPathComponent(fileName, isDirectory: false)
    return self.filemanager.fileExists(atPath: url.path)
  }
  
  // Returns URL constructed from specified directory
  fileprivate func getURL() -> URL {
    
    if let url = self.filemanager.urls(for: .documentDirectory, in: .userDomainMask).first {
      return url
      
    } else {
      fatalError("Could not create URL for specified directory!")
    }
  }
}

