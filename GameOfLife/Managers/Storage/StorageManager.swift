//
//  StorageManager.swift
//  GameOfLife
//
//  Created by Filippo Tosetto on 27/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import Foundation

class StorageManager {
  
  private let storage: Storage
  
  init(storage: Storage = Storage()) {
    self.storage = storage
  }
  
  func getAllUniverses() -> [Universe] {
    self.storage.retrieveAll(as: Universe.self)
  }
  
  func saveUniverse(_ universe: Universe) {
    self.storage.store(universe, as: "\(self.convertToFilename(universe)).json")
  }
  
  func deleteUniverse(_ universe: Universe) {
    self.storage.remove("\(self.convertToFilename(universe)).json")
  }
  
  private func convertToFilename(_ universe: Universe) -> String {
    return universe.name.replacingOccurrences(of: " ", with: "_")
  }
  
  // Used during Unit tests
  func clearAllUniverses() {
    self.storage.clear()
  }
}
