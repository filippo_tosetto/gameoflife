//
//  Cell.swift
//  GameOfLife
//
//  Created by Filippo Tosetto on 24/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import Foundation

protocol CaseIterableDefaultsLast: Codable & CaseIterable & RawRepresentable
  where RawValue: Codable, AllCases: BidirectionalCollection { }

extension CaseIterableDefaultsLast {
  init(from decoder: Decoder) throws {
    self = try Self(rawValue: decoder.singleValueContainer().decode(RawValue.self)) ?? Self.allCases.last!
  }
}

enum State: String, CaseIterableDefaultsLast {
  case alive, dead
}

struct Cell {
  
  private let x: Int
  private let y: Int
  private var state: State
  
  init(x: Int, y: Int, state: State) {
    self.x = x
    self.y = y
    self.state = state
  }
  
  func getState() -> State {
    return self.state
  }
  
  func getX() -> Int {
    return self.x
  }
  
  func getY() -> Int {
    return self.y
  }
  
  func kill() -> Cell {
    return Cell(x: self.x, y: self.y, state: .dead)
  }
  
  func resuscitate() -> Cell {
    return Cell(x: self.x, y: self.y, state: .alive)
  }
  
  func isNeighbor(to cell: Cell) -> Bool {

    // calculate distance between self and cell
    // getting the absolute value will give the same result if distance is positive or negative
    let xDelta = abs(self.x - cell.x)
    let yDelta = abs(self.y - cell.y)

    // cells are neighbor if distance between the two is 1 in any axis combination
    switch (xDelta, yDelta) {
    case (1, 1), (0, 1), (1, 0):
      return true
    default:
      return false
    }
  }
}

extension Cell: Codable, Equatable { }
