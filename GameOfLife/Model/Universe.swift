//
//  Universe.swift
//  GameOfLife
//
//  Created by Filippo Tosetto on 24/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import Foundation

struct Universe {
  
  let name: String
  
  // by having cells to know their position in the universe we just need a single array to store them
  // otherwise we would have needed a nested array to store their x and y position
  internal var cells = [Cell]()
  private var currentGeneration: Int
  
  init(name: String? = nil, cells: [Cell] = [Cell](), currentGeneration: Int = 0) {
    self.name = name ?? NameGenerator().generateName()
    self.cells = cells
    self.currentGeneration = currentGeneration
  }
  
  // size is the number of cells on X axis and Y axis
  // total number of cells is size x size
  static func create(withSize size: Int = 10, randomSeed: Int = 3) -> Universe {
    
    var cells = [Cell]()
    
    for x in 0..<size {
      for y in 0..<size {
      
        // random distribution of alive/dead cells
        // higher random seed means a lower number of alive cells
        let randomState = arc4random_uniform(UInt32(randomSeed))
        let cell = Cell(x: x, y: y, state: randomState == 0 ? .alive : .dead)
        cells.append(cell)
      }
    }
  
    return Universe(cells: cells)
  }

  func getSize() -> Int {
    return Int(sqrt(Double(self.cells.count)))
  }
  
  func getCurrentGeneration() -> Int {
    return self.currentGeneration
  }
  
  mutating func iterate() {
    
    var updatedCells = [Cell]()
    let aliveCells = self.getAliveCells()

    for cell in self.cells {
      let livingNeighbors = self.getLivingNeighbors(forCell: cell, withAliveCells: aliveCells)
      let cell = self.nextGeneration(forCell: cell, withLivingNeighbors: livingNeighbors)
      updatedCells.append(cell)
    }

    self.cells = updatedCells
    self.currentGeneration += 1
  }
}

// MARK: - Utility methods

extension Universe {
  
  // Retrieve all cells with state "alive"
  internal func getAliveCells() -> [Cell] {
    return self.cells.filter { $0.getState() == .alive }
  }
  
  // Retrieve all cells with state "alive" that are also neighbors of a given cell
  internal func getLivingNeighbors(forCell cell: Cell, withAliveCells aliveCells: [Cell]) -> [Cell] {
    return aliveCells.filter { $0.isNeighbor(to: cell) }
  }
  
  // Set new state of a give cell based on the nighbors
  internal func nextGeneration(forCell cell: Cell, withLivingNeighbors livingNeighbors: [Cell]) -> Cell {
    
    switch livingNeighbors.count {
      
    // Any alive cell with two or three living neighbors will live on to the next generation.
    case 2...3 where cell.getState() == .alive:
      return cell
      
    // Any dead cell with exactly three living neighbors will become a live cell.
    case 3 where cell.getState() == .dead:
      return cell.resuscitate()
      
    // Any live cell with fewer than two living neighbors will die.
    // Any live cell with more than three living neighbors will die.
    default:
      return cell.kill()
    }
  }
}

// MARK: - Equatable

extension Universe: Equatable, Codable {

  static func == (lhs: Universe, rhs: Universe) -> Bool {
    return
      lhs.name == rhs.name &&
      lhs.cells == rhs.cells
  }
}
