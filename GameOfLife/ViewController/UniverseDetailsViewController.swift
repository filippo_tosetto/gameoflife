//
//  UniverseDetailsViewController.swift
//  GameOfLife
//
//  Created by Filippo Tosetto on 24/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit

class UniverseDetailsViewController: UIViewController {

  @IBOutlet private weak var universeView: UniverseView!
  @IBOutlet private weak var autoplayButton: UIButton! {
    didSet {
      self.autoplayButton.setTitle("AutoPlay", for: .normal)
    }
  }
  @IBOutlet private weak var generationsLabel: UILabel! {
    didSet {
      self.generationsLabel.text = "Current Generation: 0"
    }
  }
  
  private let viewModel: UniverseViewModel
  
  init(viewModel: UniverseViewModel) {
    self.viewModel = viewModel
    
    super.init(nibName: "UniverseDetailsViewController", bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()

    self.title = self.viewModel.title()
    self.generationsLabel.text = "Current Generation: \(self.viewModel.getCurrentGeneration())"
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    self.universeView.configure(withUniverse: self.viewModel.universe, delegate: self)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    
    self.viewModel.saveUniverse(withCurrentState: self.universeView.getCurrentUniverseState())
  }
  
  // MARK: - IBActions
  
  @IBAction func stepAction(_ sender: Any) {
    self.universeView.play()
  }
  
  @IBAction func autoplayAction(_ sender: Any) {
    
    if self.universeView.isPlaying {
      self.universeView.stop()
    } else {
      self.universeView.autoPlay()
    }
    
    self.autoplayButton.setTitle(self.universeView.isPlaying ? "Stop" : "AutoPlay", for: .normal)
  }
}

// MARK: - UniverseViewDelegate

extension UniverseDetailsViewController: UniverseViewDelegate {
  
  func didUpdateGenerations(generationNumber: Int) {
    self.generationsLabel.text = "Current Generation: \(generationNumber)"
  }
}
