//
//  ListViewController.swift
//  GameOfLife
//
//  Created by Filippo Tosetto on 24/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit

class UniversesListViewController: UIViewController {

  @IBOutlet private weak var tableView: UITableView! {
    didSet {
      self.tableView.dataSource = self
      self.tableView.delegate = self
      
      self.tableView.registerNibClass(UniverseCell.self)
    }
  }
  
  private var viewModel: UniversesListViewModel
  
  init(viewModel: UniversesListViewModel) {
    self.viewModel = viewModel
    
    super.init(nibName: "UniversesListViewController", bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.title = "Game Of Life"
    self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain,
                                                             target: self, action: #selector(self.addNewUniverse))
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    self.viewModel.refreshData()
    self.tableView.reloadData()
  }
  
  @objc func addNewUniverse() {
    self.viewModel.createNewUniverse()
  }
}

// MARK: - UITableViewDataSource

extension UniversesListViewController: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.viewModel.numberOfItems()
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeReusableCellWithClass(UniverseCell.self, forIndexPath: indexPath)!
    
    if let item = self.viewModel.itemAtIndex(indexPath.row) {
      cell.configure(withUnivers: item)
    }
    
    return cell
  }
}

// MARK: - UITableViewDelegate

extension UniversesListViewController: UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    self.viewModel.showDetailsForItem(atIndex: indexPath.row)
  }
  
  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    guard editingStyle == .delete else {
      return
    }
    
    self.viewModel.deleteItem(atIndex: indexPath.row)
    tableView.deleteRows(at: [indexPath], with: .fade)
  }
}
