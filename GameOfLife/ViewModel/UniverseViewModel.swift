//
//  UniverseViewModel.swift
//  GameOfLife
//
//  Created by Filippo Tosetto on 24/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import Foundation

struct UniverseViewModel {
  
  var didTapBack: (()->())?

  let universe: Universe
  private let storageManager = StorageManager()
  
  init(universe: Universe) {
    self.universe = universe
  }
  
  // MARK: - Data
  
  func title() -> String {
    return self.universe.name
  }
  
  func getCurrentGeneration() -> Int {
    return self.universe.getCurrentGeneration()
  }
  
  // MARK: - Action
  
  // Save current state of the universe
  func saveUniverse(withCurrentState universeState: Universe?) {
    guard let state = universeState else {
      return
    }
    self.storageManager.deleteUniverse(self.universe)
    self.storageManager.saveUniverse(state)
  }
  
  func back() {
    self.didTapBack?()
  }
}
