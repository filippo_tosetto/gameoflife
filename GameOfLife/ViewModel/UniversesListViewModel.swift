//
//  UniversesListViewModel.swift
//  GameOfLife
//
//  Created by Filippo Tosetto on 24/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import Foundation

struct UniversesListViewModel {
  
  private let storageManager: StorageManager
  private var universesList = [Universe]()
  
  var didSelect: ((Universe)->())?
  
  init() {
    self.storageManager = StorageManager()
  }
  
  mutating func refreshData() {
    self.universesList = self.storageManager.getAllUniverses()
  }
  
  // This method is used only for unit test purpose
  mutating func addUniverse(_ universe: Universe) {
    self.universesList.append(universe)
  }
  
  // MARK: - Data Source
  
  func numberOfItems() -> Int {
    return self.universesList.count
  }
  
  func itemAtIndex(_ index: Int) -> Universe? {
    return self.universesList[safe: index]
  }
  
  mutating func deleteItem(atIndex idx: Int) {
    guard let item = self.itemAtIndex(idx) else {
      return
    }
    
    self.storageManager.deleteUniverse(item)
    self.universesList.remove(at: idx)
  }
  
  // MARK: - Actions
  
  func showDetailsForItem(atIndex index: Int) {
    guard let item = self.itemAtIndex(index) else  {
      return
    }
    
    self.didSelect?(item)
  }
    
  func createNewUniverse() {
    
    let universe = Universe.create(withSize: 20, randomSeed: 3)
    self.storageManager.saveUniverse(universe)
    self.didSelect?(universe)
  }
}
