//
//  UniverseCell.swift
//  GameOfLife
//
//  Created by Filippo Tosetto on 24/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit

class UniverseCell: UITableViewCell {

  @IBOutlet weak var universeNameLabel: UILabel!

  func configure(withUnivers universe: Universe) {
    self.universeNameLabel.text = universe.name
  }
}
