//
//  UniverseView.swift
//  GameOfLife
//
//  Created by Filippo Tosetto on 24/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import UIKit

protocol UniverseViewDelegate: class {
  func didUpdateGenerations(generationNumber: Int)
}

class UniverseView: UIView {

  private var universe: Universe?
  private var cellSize: Int?
  
  private weak var delegate: UniverseViewDelegate?
  
  var isPlaying = false
    
  func configure(withUniverse universe: Universe, delegate: UniverseViewDelegate?) {
    self.universe = universe
    self.cellSize = universe.getSize()
    
    self.delegate = delegate
  }
  
  override func draw(_ rect: CGRect) {
    guard let universe = self.universe, let cellSize = self.cellSize else {
      return
    }
    
    let context = UIGraphicsGetCurrentContext()
    context?.saveGState()
    
    for cell in universe.cells {
      let rect = CGRect(x: cell.getX() * cellSize, y: cell.getY() * cellSize, width: cellSize, height: cellSize)
      let color = cell.getState() == .alive ? UIColor.red.cgColor : UIColor.white.cgColor
      
      context?.addRect(rect)
      context?.setFillColor(color)
      context?.fill(rect)
    }
    
    context?.restoreGState()
  }
    
  func autoPlay() {
    self.isPlaying = true
    
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
    
      self.play()
    
      if self.isPlaying {
        self.autoPlay()
      }
    }
  }
  
  func stop() {
    self.isPlaying = false
  }
    
  func play() {
    self.universe?.iterate()
    if let universe = self.universe {
      self.delegate?.didUpdateGenerations(generationNumber: universe.getCurrentGeneration())
    }
    self.setNeedsDisplay()
  }
  
  func getCurrentUniverseState() -> Universe? {
    return self.universe
  }
}
