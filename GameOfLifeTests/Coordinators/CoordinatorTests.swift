//
//  CoordinatorTests.swift
//  GameOfLifeTests
//
//  Created by Filippo Tosetto on 24/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import XCTest
@testable import GameOfLife

class CoordinatorTests: XCTestCase {

  private let window = UIWindow()

  override func setUpWithError() throws {}
  override func tearDownWithError() throws {}

  func test_applicationCoordinator() throws {
    
    let appCoordinator = ApplicationCoordinator(window: self.window)
    appCoordinator.start()
    
    XCTAssertNotNil(self.window.rootViewController)
    XCTAssertEqual(appCoordinator.childCoordinators.count, 1)
    
    let viewControllers = (self.window.rootViewController as! UINavigationController).viewControllers
    XCTAssertEqual(viewControllers.count, 1)
  }

  func test_mainCoordinator() throws {
    
    let appCoordinator = ApplicationCoordinator(window: self.window)
    appCoordinator.start()
    
    let mainCoordinator = appCoordinator.childCoordinators.first as? MainCoordinator
    XCTAssertNotNil(mainCoordinator)
    XCTAssertEqual(mainCoordinator?.childCoordinators.count, 0)
    
    mainCoordinator?.viewModel.addUniverse(Universe(name: "A"))
    mainCoordinator?.viewModel.addUniverse(Universe(name: "B"))
    mainCoordinator?.viewModel.addUniverse(Universe(name: "C"))
    
    mainCoordinator?.viewModel.showDetailsForItem(atIndex: 0)
    
    XCTAssertEqual(mainCoordinator?.childCoordinators.count, 1)
  }
    
  func test_detailsCoordinator() throws {
    let appCoordinator = ApplicationCoordinator(window: self.window)
    appCoordinator.start()

    let mainCoordinator = appCoordinator.childCoordinators.first as! MainCoordinator
    
    mainCoordinator.viewModel.addUniverse(Universe(name: "A"))
    mainCoordinator.viewModel.addUniverse(Universe(name: "B"))
    mainCoordinator.viewModel.addUniverse(Universe(name: "C"))
    
    mainCoordinator.viewModel.showDetailsForItem(atIndex: 0)

    let detailsCoordinator = mainCoordinator.childCoordinators.first as? DetailsCoordinator
    XCTAssertNotNil(detailsCoordinator)
    XCTAssertEqual(detailsCoordinator?.childCoordinators.count, 0)
    
    detailsCoordinator?.viewModel.back()
    
    XCTAssertEqual(mainCoordinator.childCoordinators.count, 0)
  }
}

