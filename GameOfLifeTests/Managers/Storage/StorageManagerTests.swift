//
//  StorageManagerTests.swift
//  GameOfLifeTests
//
//  Created by Filippo Tosetto on 27/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import XCTest
@testable import GameOfLife

class StorageManagerTests: XCTestCase {

  private let storageManager = StorageManager()
  
  override func setUpWithError() throws {
    self.storageManager.clearAllUniverses()
  }
  override func tearDownWithError() throws {
    self.storageManager.clearAllUniverses()
  }

  func test_save() throws {
   
    var allUniverses = self.storageManager.getAllUniverses()
    XCTAssertTrue(allUniverses.count == 0)
    
    let universe = Universe(name: "universe", cells: [Cell]())
    self.storageManager.saveUniverse(universe)
    allUniverses = self.storageManager.getAllUniverses()
    XCTAssertTrue(allUniverses.count == 1)
  }
  
  func test_delete() throws {
    
    let universe_1 = Universe(name: "universe 1", cells: [Cell]())
    let universe_2 = Universe(name: "universe 2", cells: [Cell]())
    let universe_3 = Universe(name: "universe 3", cells: [Cell]())
    self.storageManager.saveUniverse(universe_1)
    self.storageManager.saveUniverse(universe_2)
    self.storageManager.saveUniverse(universe_3)
    
    var allUniverses = self.storageManager.getAllUniverses()
    XCTAssertTrue(allUniverses.count == 3)
    
    self.storageManager.deleteUniverse(universe_1)
    
    allUniverses = self.storageManager.getAllUniverses()
    XCTAssertTrue(allUniverses.count == 2)
  }
}
