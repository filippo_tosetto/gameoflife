//
//  StorageTests.swift
//  GameOfLifeTests
//
//  Created by Filippo Tosetto on 27/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import XCTest
@testable import GameOfLife

class StorageTests: XCTestCase {

  private let storage = Storage()
  
  override func setUpWithError() throws {
    self.storage.clear()
  }
  
  override func tearDownWithError() throws {
    self.storage.clear()
  }

  func test_store() throws {
    
    let item_1 = Universe(cells: [])
    let item_2 = Universe(cells: [])
    let item_3 = Universe(cells: [])
    
    self.storage.store(item_1, as: item_1.name)
    self.storage.store(item_2, as: item_2.name)
    self.storage.store(item_3, as: item_3.name)
    
    var storedItems = self.storage.retrieveAll(as: Universe.self)
    XCTAssertTrue(storedItems.count == 3)
    
    let retrievedItem_1 = self.storage.retrieve(item_1.name, as: Universe.self)
    
    XCTAssertEqual(item_1, retrievedItem_1)
    
    self.storage.remove(item_1.name)
    
    storedItems = self.storage.retrieveAll(as: Universe.self)
    XCTAssertTrue(storedItems.count == 2)
  }
}
