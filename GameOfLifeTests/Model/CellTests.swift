//
//  CellTests.swift
//  GameOfLifeTests
//
//  Created by Filippo Tosetto on 24/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import XCTest
@testable import GameOfLife

class CellTests: XCTestCase {

  override func setUpWithError() throws { }
  override func tearDownWithError() throws {}

  func test_isNeighbour() throws {
    
    // distance is (1, 1)
    var firstCell = Cell(x: 10, y: 10, state: .dead)
    var secondCell = Cell(x: 11, y: 11, state: .dead)
    
    XCTAssertTrue(firstCell.isNeighbor(to: secondCell))
    
    // distance is (1, 0)
    firstCell = Cell(x: 10, y: 10, state: .dead)
    secondCell = Cell(x: 11, y: 10, state: .dead)
    
    XCTAssertTrue(firstCell.isNeighbor(to: secondCell))
    
    // distance is (0, 1)
    firstCell = Cell(x: 10, y: 10, state: .dead)
    secondCell = Cell(x: 10, y: 11, state: .dead)
    
    XCTAssertTrue(firstCell.isNeighbor(to: secondCell))
    
    // distance is (0, -1)
    firstCell = Cell(x: 10, y: 10, state: .dead)
    secondCell = Cell(x: 10, y: 9, state: .dead)
    
    XCTAssertTrue(firstCell.isNeighbor(to: secondCell))
    
    // distance is (-1, -1)
    firstCell = Cell(x: 10, y: 10, state: .dead)
    secondCell = Cell(x: 9, y: 9, state: .dead)
    
    XCTAssertTrue(firstCell.isNeighbor(to: secondCell))

  }
  
  func test_isNotNeighbour() throws {
      
    // distance is (10, 0)
    var firstCell = Cell(x: 10, y: 10, state: .dead)
    var secondCell = Cell(x: 20, y: 10, state: .dead)
    
    XCTAssertFalse(firstCell.isNeighbor(to: secondCell))

    // distance is (0, 0)
    firstCell = Cell(x: 10, y: 10, state: .dead)
    secondCell = Cell(x: 10, y: 10, state: .dead)
    
    XCTAssertFalse(firstCell.isNeighbor(to: secondCell))
    
    // distance is (1, 10)
    firstCell = Cell(x: 10, y: 10, state: .dead)
    secondCell = Cell(x: 11, y: 0, state: .dead)
    
    XCTAssertFalse(firstCell.isNeighbor(to: secondCell))
  }
  
  func test_equatable() throws {
    
    var firstCell = Cell(x: 10, y: 10, state: .alive)
    var secondCell = Cell(x: 10, y: 10, state: .alive)
    
    XCTAssertEqual(firstCell, secondCell)
    
    firstCell = Cell(x: 10, y: 10, state: .dead)
    secondCell = Cell(x: 10, y: 10, state: .dead)
    
    XCTAssertEqual(firstCell, secondCell)
  }
  
  func test_inequality() throws {
    
    var firstCell = Cell(x: 10, y: 11, state: .alive)
    var secondCell = Cell(x: 10, y: 10, state: .alive)
    
    XCTAssertNotEqual(firstCell, secondCell)
    
    firstCell = Cell(x: 10, y: 10, state: .alive)
    secondCell = Cell(x: 10, y: 10, state: .dead)
    
    XCTAssertNotEqual(firstCell, secondCell)
  }
  
  func test_getState() throws {
    
    var cell = Cell(x: 10, y: 11, state: .alive)
    XCTAssertTrue(cell.getState() == .alive)
    
    cell = Cell(x: 10, y: 11, state: .dead)
    XCTAssertTrue(cell.getState() == .dead)
    
    cell = Cell(x: 10, y: 11, state: .dead)
    XCTAssertFalse(cell.getState() == .alive)
  }
  
  func test_kill() throws {
    
    var cell = Cell(x: 10, y: 11, state: .alive)
    var killedCell = cell.kill()
    
    // Alive cell will become dead
    XCTAssertTrue(killedCell.getState() == .dead)

    
    // Dead cell will stay dead
    cell = Cell(x: 10, y: 11, state: .dead)
    killedCell = cell.kill()
    
    XCTAssertTrue(killedCell.getState() == .dead)
  }

  func test_resuscitate() throws {
    
    var cell = Cell(x: 10, y: 11, state: .dead)
    var aliveCell = cell.resuscitate()
    
    // Dead cell will become alive
    XCTAssertTrue(aliveCell.getState() == .alive)

    
    cell = Cell(x: 10, y: 11, state: .alive)
    aliveCell = cell.resuscitate()
    
    // Alive cell will become dead
    XCTAssertTrue(aliveCell.getState() == .alive)
  }
}
