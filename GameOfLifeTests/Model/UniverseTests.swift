//
//  UniverseTests.swift
//  GameOfLifeTests
//
//  Created by Filippo Tosetto on 24/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import XCTest
@testable import GameOfLife

class UniverseTests: XCTestCase {

  override func setUpWithError() throws { }
  override func tearDownWithError() throws { }

  func test_equatable() throws {
    let firstCell = Cell(x: 1, y: 1, state: .alive)
    let secondCell = Cell(x: 1, y: 1, state: .alive)
    
    let firstUniverse = Universe(name: "A", cells: [firstCell, secondCell])
    let secondUniverse = Universe(name: "A", cells: [firstCell, secondCell])
    
    XCTAssertEqual(firstUniverse, secondUniverse)
  }
  
  func test_unequatable() throws {
    let firstCell = Cell(x: 1, y: 1, state: .alive)
    let secondCell = Cell(x: 1, y: 1, state: .alive)

    let firstUniverse = Universe(name: "A")
    let secondUniverse = Universe(name: "B")
    
    XCTAssertNotEqual(firstUniverse, secondUniverse)
    
    let thirdUniverse = Universe(name: "A", cells: [firstCell, secondCell])
    let forthUniverse = Universe(name: "A", cells: [firstCell])
    
    XCTAssertNotEqual(thirdUniverse, forthUniverse)
  }
  
  func test_creation() throws {
    let universe = Universe.create(withSize: 10, randomSeed: 2)
    XCTAssertEqual(universe.cells.count, 100)
  
    // Two universes generated from random seeds are never the same as the cells are generated randomly
    let firstUniverse = Universe.create(withSize: 10, randomSeed: 2)
    let secondUniverse = Universe.create(withSize: 10, randomSeed: 2)
    XCTAssertNotEqual(firstUniverse, secondUniverse)
  }
  
  func test_size() throws {
    let universe = Universe.create(withSize: 10, randomSeed: 2)
    XCTAssertEqual(universe.getSize(), 10)
  }
  
  func test_currentGeneration() throws {

    var universe = Universe()
    XCTAssertTrue(universe.getCurrentGeneration() == 0)
    
    universe.iterate()
    XCTAssertTrue(universe.getCurrentGeneration() == 1)
    
    universe.iterate()
    universe.iterate()
    XCTAssertTrue(universe.getCurrentGeneration() == 3)
  }
  
  func test_aliveCells() throws {
    let aliveCellOne = Cell(x: 0, y: 0, state: .alive)
    let aliveCellTwo = Cell(x: 1, y: 0, state: .alive)

    let cells = [
      aliveCellOne, aliveCellTwo,
      Cell(x: 0, y: 1, state: .dead),
      Cell(x: 1, y: 1, state: .dead)
    ]
    
    let universe = Universe(name: "A", cells: cells)
    let aliveCells = universe.getAliveCells()
    
    XCTAssertEqual(aliveCells[0], aliveCellOne)
    XCTAssertEqual(aliveCells[1], aliveCellTwo)
  }
  
  func test_livingNeighbors() throws {
    
    let one = Cell(x: 0, y: 0, state: .alive)
    let two = Cell(x: 1, y: 0, state: .alive)
    let three = Cell(x: 2, y: 0, state: .dead)
    let four = Cell(x: 0, y: 1, state: .dead)
    let five = Cell(x: 1, y: 1, state: .alive)
    let six = Cell(x: 2, y: 1, state: .dead)
    let seven = Cell(x: 0, y: 2, state: .alive)
    let eight = Cell(x: 1, y: 2, state: .dead)
    let nine = Cell(x: 2, y: 2, state: .alive)

    let cells = [one, two, three, four, five, six, seven, eight, nine]
    
    let universe = Universe(cells: cells)
    let aliveCells = universe.getAliveCells()
    
    let cell = Cell(x: 1, y: 1, state: .alive) // our cell is actually "five"
    let livingNeighbors = universe.getLivingNeighbors(forCell: cell, withAliveCells: aliveCells)
    
    // even tho there are 5 cells alive, as one of them is our current cell the neighbors are only 4
    XCTAssertTrue(livingNeighbors.count == 4)
  }
  
  func test_newGeneration() throws {
    
    // Test: Any live cell with fewer than two living neighbors will die.
    // 00
    // XX
    var one = Cell(x: 0, y: 0, state: .alive)
    var two = Cell(x: 1, y: 0, state: .alive)
    var three = Cell(x: 0, y: 1, state: .dead)
    var four = Cell(x: 1, y: 1, state: .dead)

    var universe = Universe(cells: [one, two, three, four])
    let newOne = universe.nextGeneration(forCell: one, withLivingNeighbors: [two])
    
    // cell "one" has only one living neighbor, two.
    // cell "one" new state is dead
    XCTAssertTrue(newOne.getState() == .dead)
    
    
    // Test: Any dead cell with exactly three living neighbors will become a live cell.
    // 00
    // 0X
    one = Cell(x: 0, y: 0, state: .alive)
    two = Cell(x: 1, y: 0, state: .alive)
    three = Cell(x: 0, y: 1, state: .alive)
    four = Cell(x: 1, y: 1, state: .dead)

    universe = Universe(cells: [one, two, three, four])
    let newFour = universe.nextGeneration(forCell: four, withLivingNeighbors: [one, two, three])
    
    // cell "four" has 3 living neighbors
    // cell "four" new state is alive
    XCTAssertTrue(newFour.getState() == .alive)

    
    // Test: Any alive cell with two or three living neighbors will live on to the next generation.
    // 00
    // 00
    one = Cell(x: 0, y: 0, state: .alive)
    two = Cell(x: 1, y: 0, state: .alive)
    three = Cell(x: 0, y: 1, state: .alive)
    four = Cell(x: 1, y: 1, state: .alive)
    universe = Universe(cells: [one, two, three, four])

    let newThree = universe.nextGeneration(forCell: three, withLivingNeighbors: [one, two, four])
    XCTAssertTrue(newThree.getState() == .alive)

    
    // Test: Any live cell with more than three living neighbors will die.
    // 000
    // 000
    one = Cell(x: 0, y: 0, state: .alive)
    two = Cell(x: 1, y: 0, state: .alive)
    three = Cell(x: 2, y: 0, state: .alive)
    four = Cell(x: 0, y: 1, state: .alive)
    let five = Cell(x: 1, y: 1, state: .alive)
    let six = Cell(x: 2, y: 1, state: .alive)
    
    universe = Universe(cells: [one, two, three, four, five, six])
    let newFive = universe.nextGeneration(forCell: four, withLivingNeighbors: [one, two, three, four, six])
    XCTAssertTrue(newFive.getState() == .dead)
  }
  
  func test_iterate() throws {
    
    // 00X
    // X0X
    // 0X0
    let one = Cell(x: 0, y: 0, state: .alive)
    let two = Cell(x: 1, y: 0, state: .alive)
    let three = Cell(x: 2, y: 0, state: .dead)
    let four = Cell(x: 0, y: 1, state: .dead)
    let five = Cell(x: 1, y: 1, state: .alive)
    let six = Cell(x: 2, y: 1, state: .dead)
    let seven = Cell(x: 0, y: 2, state: .alive)
    let eight = Cell(x: 1, y: 2, state: .dead)
    let nine = Cell(x: 2, y: 2, state: .alive)
    
    let cells = [one, two, three, four, five, six, seven, eight, nine]
    
    var universe = Universe(cells: cells)
    
    // First Iteration
    universe.iterate()
    var aliveCells = universe.getAliveCells()
    
    // 00X
    // XX0
    // X0X
    XCTAssertTrue(aliveCells.count == 4)
    XCTAssertTrue(universe.getCurrentGeneration() == 1)
    XCTAssertEqual(aliveCells[0], Cell(x: 0, y: 0, state: .alive))
    XCTAssertEqual(aliveCells[1], Cell(x: 1, y: 0, state: .alive))
    XCTAssertEqual(aliveCells[2], Cell(x: 2, y: 1, state: .alive))
    XCTAssertEqual(aliveCells[3], Cell(x: 1, y: 2, state: .alive))
    
    // Second Iteration
    universe.iterate()
    aliveCells = universe.getAliveCells()
    
    // X0X
    // 0X0
    // XXX
    XCTAssertTrue(aliveCells.count == 3)
    XCTAssertTrue(universe.getCurrentGeneration() == 2)
    XCTAssertEqual(aliveCells[0], Cell(x: 1, y: 0, state: .alive))
    XCTAssertEqual(aliveCells[1], Cell(x: 0, y: 1, state: .alive))
    XCTAssertEqual(aliveCells[2], Cell(x: 2, y: 1, state: .alive))
  }
}
