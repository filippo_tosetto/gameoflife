//
//  UniverseViewModelTests.swift
//  GameOfLifeTests
//
//  Created by Filippo Tosetto on 24/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import XCTest
@testable import GameOfLife

class UniverseViewModelTests: XCTestCase {

  override func setUpWithError() throws {}
  override func tearDownWithError() throws {}

  func test_init() throws {
    let universe = Universe(name: "A")
    let viewModel = UniverseViewModel(universe: universe)
    
    XCTAssertEqual(universe.name, viewModel.title())
  }
}
