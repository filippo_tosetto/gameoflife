//
//  UniversesListViewModelTest.swift
//  GameOfLifeTests
//
//  Created by Filippo Tosetto on 24/06/2020.
//  Copyright © 2020 Pizza Connection. All rights reserved.
//

import XCTest
@testable import GameOfLife

class UniversesListViewModelTest: XCTestCase {

  var viewModel: UniversesListViewModel!

  override func setUpWithError() throws {
    self.viewModel = UniversesListViewModel()
  }

  override func tearDownWithError() throws { }

  func test_numberOfObjects() throws {
    
    let numberOfItems = self.viewModel.numberOfItems()
    XCTAssertEqual(numberOfItems, 0)
    
    self.populate()
    
    let newNumberOfItems = self.viewModel.numberOfItems()
    XCTAssertEqual(newNumberOfItems, 4)
  }

  func test_objectAtIdx() throws {
    
    let data = self.populate()

    let firstItem = self.viewModel.itemAtIndex(0)
    XCTAssertEqual(firstItem, data[0])

    let secondItem = self.viewModel.itemAtIndex(1)
    XCTAssertEqual(secondItem, data[1])

    let fifthItem = self.viewModel.itemAtIndex(4)
    XCTAssertNil(fifthItem)
  }
}

// MARK: - Private Methods

extension UniversesListViewModelTest {
  
  @discardableResult
  private func populate() -> [Universe] {
    
    let data = [
      Universe(name: "A"),
      Universe(name: "B"),
      Universe(name: "C"),
      Universe(name: "D")
    ]
    data.forEach { self.viewModel.addUniverse($0) }
    
    return data
  }
}
