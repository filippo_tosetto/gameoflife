# Game of Life

Proof of concept for John Conway’s Game of Life.

## Architecture

The app tries to follow the **MVVM** archiecture where possible. For each View Controllers there's a View Model that takes care of preparing the data to be dispayed in the View. The View Controllers have little to no business logic, every _write_ operation in the data model is handled by the View Models.

### Flow Controller

The Flow of the application, so which View Controller to display and when, is handled by **Coordinators**. This approach is particularly useful when there flow of the application is quite complex. The advantage is that each View Controller is completely decoupled from every other View Controller.

### Data Model

The data model is fairly simple.
**Cell** wich have a state and a position.
And **Universe** wich contains a list of cells, and takes care of calculating the next generations.

By having each cell knowing its position within the universe, we don't need any nested loop to iterate through the list of cells, and we don't need any nested array as data structure to save them.

Each universe has an auto-generated name, which is taken randomly from a list of pre set mathematicians name.

### Storage

Persistence operations are all handled by **StorageManager**, which takes care of performing read/write/delete operations on the DataModel.

The **Storage** class takes care of reading/writing files to disk. Each file represents a universe and it's state.

By having a dedicated class to handle persistency, we can easily substitute saving files to disk with any other persistency technology, like CoreData or Realms.
In this way the rest of the application is completely decoupled from the persistent layer.

## Unit Tests

The Unit Tests project takes care of testing some parts of the application. Mainly business logic and algorithms.

There's no 100% code coverage as some parts of the application, like ViewControllers, are better tested using UI tests.

## Roadmap
The application can be improved by implementing a few other features:

* A way for the user to create/edit each universe
* For each universe, it would be nice to take a screenshot of it's saved state, and display it in the initial list alongside its name.
* A way to choose the universe size, at the moment it's fixed
* A way to change the universe name
* A more appealing/customizable UI
